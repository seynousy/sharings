import React, { Component } from 'react';
import { Form, Button, Grid } from 'semantic-ui-react';
import Dropzone from 'react-dropzone';
import Departements from '../constants/departements.json';
import Subjects from '../constants/subjects.json';
import changeCase from 'change-case';
import Formsy from 'formsy-react';

{/* FORMSY SEMANTIC UI REACT IMPORT */}

import {
  Input,
  TextArea,
  Checkbox,
  Radio,
  RadioGroup,
  Dropdown,
  Select
} from 'formsy-semantic-ui-react';

{/* FORMSY SEMANTIC UI REACT IMPORT */}



const styles = {
	paddingStyle: {
		padding: 20,
	},
	dropzoneStyle: {
		margin: 55,
	}
}

const departementsList = Departements.map( (d) => ({
		key:d.key,
		text:changeCase.upperCaseFirst(d.key),
		value:d.key
	}) 
);

const yearsList = [
	{key: 1, value: 1, text: "1"},
	{key: 2, value: 2, text: "2"},
	{key: 3, value: 3, text: "3"},
	{key: 4, value: 4, text: "4"},
	{key: 5, value: 5, text: "5"}
]

export default class extends Component {
	constructor(props) {
		super(props);
		this.reloadSubjects = this.reloadSubjects.bind(this);
		this.makeChanges = this.makeChanges.bind(this);
		this.state = {
			selectedDepartement: "",
			subjectsLizt: [],
		}
	}

	reloadSubjects(departement) {
		let subjectsList = Subjects.find((element) => element.departement==departement);
		this.setState({subjectsLizt: subjectsList.subjects});
		let subjectsLizt = subjectsList.subjects;
		// console.log(subjectsLizt);
		this.setState({
			subjectsLizt: subjectsLizt.map( (s) => ({
				key: s.key,
				text: changeCase.upperCaseFirst(s.key),
				value: s.key
			}))
		})
	}

	makeChanges(currentValues) {
		this.reloadSubjects(currentValues.departement);
	}

	render() {
		return(
			<Grid centered>
				<Grid.Column width={5}>
					<Form>
						<Formsy.Form
							onChange={this.makeChanges}
						>
							<Form.Field>
								<Input name="title" placeholder='Khint Algo' icon='users' iconPosition='left' />
							</Form.Field>
							<Form.Field>
								<Grid centered  style={styles.paddingStyle}>	
									<Dropzone>
										<div style={styles.dropzoneStyle}>Glissez ou Sélectionner</div>
									</Dropzone>
								</Grid>
							</Form.Field>
							<Form.Field>
								<TextArea name="comments" placeholder="Commentaires"/>
							</Form.Field>
							<Form.Field>
								<Select
									placeholder="Départements"
									name="departement"
									options={departementsList}
								/>
							</Form.Field>
							<Form.Field>
								<Select
									name="level"
									placeholder="Niveau (année)"
									options={yearsList}
								/>
							</Form.Field>
							<Form.Field>
								<Select
									name="option"
									placeholder="Matière"
									options={this.state.subjectsLizt}
								/>
							</Form.Field>
						</Formsy.Form>
					</Form>
				</Grid.Column>
			</Grid>
		)
	}
}